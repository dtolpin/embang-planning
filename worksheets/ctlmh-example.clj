;; gorilla-repl.fileformat = 1

;; **
;;; # Control LMH, an illustration
;; **

;; @@
(ns ctlmh-example
  (:require [gorilla-plot.core :as plot])
  (:use [embang runtime emit 
         [core :only [doquery]]
         [state :only [get-predicts]]]))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-nil'>nil</span>","value":"nil"}
;; <=

;; **
;;; Let's see how CTLMH affects inference. We compare two queries for normal with unknown parameters.  Let's first define the model parameters.
;; **

;; @@
(def data [1 1 0.9 1.1 0.95 1.05])
(def mean-prior (normal 0. 1.))
(def prec-prior (gamma 1. 1.))
;; @@
;; =>
;;; {"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"html","content":"<span class='clj-var'>#&#x27;ctlmh-example/data</span>","value":"#'ctlmh-example/data"},{"type":"html","content":"<span class='clj-var'>#&#x27;ctlmh-example/mean-prior</span>","value":"#'ctlmh-example/mean-prior"}],"value":"[#'ctlmh-example/data,#'ctlmh-example/mean-prior]"},{"type":"html","content":"<span class='clj-var'>#&#x27;ctlmh-example/prec-prior</span>","value":"#'ctlmh-example/prec-prior"}],"value":"[[#'ctlmh-example/data,#'ctlmh-example/mean-prior],#'ctlmh-example/prec-prior]"}
;; <=

;; **
;;; In the first one, both mean and sd are given priors.
;; **

;; @@

(defquery normal-unknown-mean-sd
  (let [mean (sample mean-prior)
        sd (sqrt (/ (sample prec-prior)))
        dist (normal mean sd)]
    (reduce (fn [_ x] (observe dist x)) nil data)
    (predict :mean mean)
    (predict :sd sd)))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;ctlmh-example/normal-unknown-mean-sd</span>","value":"#'ctlmh-example/normal-unknown-mean-sd"}
;; <=

;; **
;;; In the second one, the sd does not converge, only the mean drifts.
;; **

;; @@
(defquery normal-unknown-mean+sd
  (let [mean (sample mean-prior)
        sd (sqrt (/ (sample (with-meta 'sd {:noise true}) prec-prior)))
        dist (normal mean sd)]
    (reduce (fn [_ x] (observe dist x)) nil data)
    (predict :mean mean)
    (predict :sd sd)))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;ctlmh-example/normal-unknown-mean+sd</span>","value":"#'ctlmh-example/normal-unknown-mean+sd"}
;; <=

;; @@
(def N 5000)
(def T 20)
(def mean-sd (map get-predicts 
                  (take N (drop N (take-nth (quot T 2) (doquery :ctlmh normal-unknown-mean-sd nil))))))
(def mean+sd (map get-predicts 
                  (take N (drop N (take-nth (quot T 2) (doquery :ctlmh normal-unknown-mean+sd nil))))))
;; @@
;; =>
;;; {"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"html","content":"<span class='clj-var'>#&#x27;ctlmh-example/N</span>","value":"#'ctlmh-example/N"},{"type":"html","content":"<span class='clj-var'>#&#x27;ctlmh-example/T</span>","value":"#'ctlmh-example/T"}],"value":"[#'ctlmh-example/N,#'ctlmh-example/T]"},{"type":"html","content":"<span class='clj-var'>#&#x27;ctlmh-example/mean-sd</span>","value":"#'ctlmh-example/mean-sd"}],"value":"[[#'ctlmh-example/N,#'ctlmh-example/T],#'ctlmh-example/mean-sd]"},{"type":"html","content":"<span class='clj-var'>#&#x27;ctlmh-example/mean+sd</span>","value":"#'ctlmh-example/mean+sd"}],"value":"[[[#'ctlmh-example/N,#'ctlmh-example/T],#'ctlmh-example/mean-sd],#'ctlmh-example/mean+sd]"}
;; <=

;; @@
[(plot/histogram (map :mean mean-sd)
                 :bins 25 :plot-size 300 :plot-range [[-2 2] :all] :y-title "sample count" :x-title "mean")
 (plot/histogram (map :sd mean-sd)
                 :bins 25 :plot-size 300 :plot-range [[0 5] :all] :x-title "sd")]

;; @@

;; @@
[(plot/histogram (map :mean mean+sd)
                 :bins 25 :plot-size 300 :plot-range [[-2 2] :all] :y-title "sample count" :x-title "mean")
 (plot/histogram (map :sd mean+sd) 
                 :bins 25 :plot-size 300 :plot-range [[0 5] :all] :x-title "sd")]

;; @@
;; =>
;;; {"type":"list-like","open":"<span class='clj-vector'>[</span>","close":"<span class='clj-vector'>]</span>","separator":" ","items":[{"type":"vega","content":{"axes":[{"titleOffset":30,"title":"mean","scale":"x","type":"x"},{"titleOffset":45,"title":"sample count","scale":"y","type":"y"}],"scales":[{"name":"x","type":"linear","range":"width","zero":false,"domain":[-2,2]},{"name":"y","type":"linear","range":"height","nice":true,"zero":false,"domain":{"data":"0446bbc0-fb98-4ec7-a481-d1ce0af206fd","field":"data.y"}}],"marks":[{"type":"line","from":{"data":"0446bbc0-fb98-4ec7-a481-d1ce0af206fd"},"properties":{"enter":{"x":{"scale":"x","field":"data.x"},"y":{"scale":"y","field":"data.y"},"interpolate":{"value":"step-before"},"fill":{"value":"steelblue"},"fillOpacity":{"value":0.4},"stroke":{"value":"steelblue"},"strokeWidth":{"value":2},"strokeOpacity":{"value":1}}}}],"data":[{"name":"0446bbc0-fb98-4ec7-a481-d1ce0af206fd","values":[{"x":-2.0,"y":0},{"x":-1.84,"y":12.0},{"x":-1.6800000000000002,"y":19.0},{"x":-1.5200000000000002,"y":24.0},{"x":-1.3600000000000003,"y":37.0},{"x":-1.2000000000000004,"y":49.0},{"x":-1.0400000000000005,"y":59.0},{"x":-0.8800000000000004,"y":117.0},{"x":-0.7200000000000004,"y":177.0},{"x":-0.5600000000000004,"y":227.0},{"x":-0.40000000000000036,"y":354.0},{"x":-0.24000000000000035,"y":484.0},{"x":-0.08000000000000035,"y":677.0},{"x":0.07999999999999965,"y":917.0},{"x":0.23999999999999966,"y":1439.0},{"x":0.3999999999999997,"y":1937.0},{"x":0.5599999999999997,"y":2491.0},{"x":0.7199999999999998,"y":2986.0},{"x":0.8799999999999998,"y":3149.0},{"x":1.0399999999999998,"y":3043.0},{"x":1.1999999999999997,"y":2549.0},{"x":1.3599999999999997,"y":1756.0},{"x":1.5199999999999996,"y":1101.0},{"x":1.6799999999999995,"y":652.0},{"x":1.8399999999999994,"y":361.0},{"x":1.9999999999999993,"y":179.0},{"x":2.1599999999999993,"y":101.0},{"x":2.3199999999999994,"y":0}]}],"width":300,"height":185.41409301757812,"padding":{"bottom":40,"top":10,"right":10,"left":55}},"value":"#gorilla_repl.vega.VegaView{:content {:axes [{:titleOffset 30, :title \"mean\", :scale \"x\", :type \"x\"} {:titleOffset 45, :title \"sample count\", :scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"linear\", :range \"width\", :zero false, :domain [-2 2]} {:name \"y\", :type \"linear\", :range \"height\", :nice true, :zero false, :domain {:data \"0446bbc0-fb98-4ec7-a481-d1ce0af206fd\", :field \"data.y\"}}], :marks [{:type \"line\", :from {:data \"0446bbc0-fb98-4ec7-a481-d1ce0af206fd\"}, :properties {:enter {:x {:scale \"x\", :field \"data.x\"}, :y {:scale \"y\", :field \"data.y\"}, :interpolate {:value \"step-before\"}, :fill {:value \"steelblue\"}, :fillOpacity {:value 0.4}, :stroke {:value \"steelblue\"}, :strokeWidth {:value 2}, :strokeOpacity {:value 1}}}}], :data [{:name \"0446bbc0-fb98-4ec7-a481-d1ce0af206fd\", :values ({:x -2.0, :y 0} {:x -1.84, :y 12.0} {:x -1.6800000000000002, :y 19.0} {:x -1.5200000000000002, :y 24.0} {:x -1.3600000000000003, :y 37.0} {:x -1.2000000000000004, :y 49.0} {:x -1.0400000000000005, :y 59.0} {:x -0.8800000000000004, :y 117.0} {:x -0.7200000000000004, :y 177.0} {:x -0.5600000000000004, :y 227.0} {:x -0.40000000000000036, :y 354.0} {:x -0.24000000000000035, :y 484.0} {:x -0.08000000000000035, :y 677.0} {:x 0.07999999999999965, :y 917.0} {:x 0.23999999999999966, :y 1439.0} {:x 0.3999999999999997, :y 1937.0} {:x 0.5599999999999997, :y 2491.0} {:x 0.7199999999999998, :y 2986.0} {:x 0.8799999999999998, :y 3149.0} {:x 1.0399999999999998, :y 3043.0} {:x 1.1999999999999997, :y 2549.0} {:x 1.3599999999999997, :y 1756.0} {:x 1.5199999999999996, :y 1101.0} {:x 1.6799999999999995, :y 652.0} {:x 1.8399999999999994, :y 361.0} {:x 1.9999999999999993, :y 179.0} {:x 2.1599999999999993, :y 101.0} {:x 2.3199999999999994, :y 0})}], :width 300, :height 185.4141, :padding {:bottom 40, :top 10, :right 10, :left 55}}}"},{"type":"vega","content":{"axes":[{"titleOffset":30,"title":"sd","scale":"x","type":"x"},{"scale":"y","type":"y"}],"scales":[{"name":"x","type":"linear","range":"width","zero":false,"domain":[0,5]},{"name":"y","type":"linear","range":"height","nice":true,"zero":false,"domain":{"data":"e53e54dd-6bf5-4e9f-b622-50dd0e5ec19f","field":"data.y"}}],"marks":[{"type":"line","from":{"data":"e53e54dd-6bf5-4e9f-b622-50dd0e5ec19f"},"properties":{"enter":{"x":{"scale":"x","field":"data.x"},"y":{"scale":"y","field":"data.y"},"interpolate":{"value":"step-before"},"fill":{"value":"steelblue"},"fillOpacity":{"value":0.4},"stroke":{"value":"steelblue"},"strokeWidth":{"value":2},"strokeOpacity":{"value":1}}}}],"data":[{"name":"e53e54dd-6bf5-4e9f-b622-50dd0e5ec19f","values":[{"x":0.0,"y":0},{"x":0.20000000000000004,"y":0.0},{"x":0.4000000000000001,"y":47.0},{"x":0.6000000000000001,"y":1492.0},{"x":0.8000000000000002,"y":3784.0},{"x":1.0000000000000002,"y":3951.0},{"x":1.2000000000000002,"y":3279.0},{"x":1.4000000000000001,"y":2478.0},{"x":1.6,"y":1883.0},{"x":1.8,"y":1489.0},{"x":2.0,"y":1076.0},{"x":2.2,"y":899.0},{"x":2.4000000000000004,"y":658.0},{"x":2.6000000000000005,"y":579.0},{"x":2.8000000000000007,"y":461.0},{"x":3.000000000000001,"y":342.0},{"x":3.200000000000001,"y":296.0},{"x":3.4000000000000012,"y":255.0},{"x":3.6000000000000014,"y":237.0},{"x":3.8000000000000016,"y":192.0},{"x":4.000000000000002,"y":145.0},{"x":4.200000000000002,"y":125.0},{"x":4.400000000000002,"y":109.0},{"x":4.600000000000002,"y":96.0},{"x":4.8000000000000025,"y":89.0},{"x":5.000000000000003,"y":91.0},{"x":5.200000000000003,"y":0}]}],"width":300,"height":185.41409301757812,"padding":{"bottom":40,"top":10,"right":10,"left":55}},"value":"#gorilla_repl.vega.VegaView{:content {:axes [{:titleOffset 30, :title \"sd\", :scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"linear\", :range \"width\", :zero false, :domain [0 5]} {:name \"y\", :type \"linear\", :range \"height\", :nice true, :zero false, :domain {:data \"e53e54dd-6bf5-4e9f-b622-50dd0e5ec19f\", :field \"data.y\"}}], :marks [{:type \"line\", :from {:data \"e53e54dd-6bf5-4e9f-b622-50dd0e5ec19f\"}, :properties {:enter {:x {:scale \"x\", :field \"data.x\"}, :y {:scale \"y\", :field \"data.y\"}, :interpolate {:value \"step-before\"}, :fill {:value \"steelblue\"}, :fillOpacity {:value 0.4}, :stroke {:value \"steelblue\"}, :strokeWidth {:value 2}, :strokeOpacity {:value 1}}}}], :data [{:name \"e53e54dd-6bf5-4e9f-b622-50dd0e5ec19f\", :values ({:x 0.0, :y 0} {:x 0.20000000000000004, :y 0.0} {:x 0.4000000000000001, :y 47.0} {:x 0.6000000000000001, :y 1492.0} {:x 0.8000000000000002, :y 3784.0} {:x 1.0000000000000002, :y 3951.0} {:x 1.2000000000000002, :y 3279.0} {:x 1.4000000000000001, :y 2478.0} {:x 1.6, :y 1883.0} {:x 1.8, :y 1489.0} {:x 2.0, :y 1076.0} {:x 2.2, :y 899.0} {:x 2.4000000000000004, :y 658.0} {:x 2.6000000000000005, :y 579.0} {:x 2.8000000000000007, :y 461.0} {:x 3.000000000000001, :y 342.0} {:x 3.200000000000001, :y 296.0} {:x 3.4000000000000012, :y 255.0} {:x 3.6000000000000014, :y 237.0} {:x 3.8000000000000016, :y 192.0} {:x 4.000000000000002, :y 145.0} {:x 4.200000000000002, :y 125.0} {:x 4.400000000000002, :y 109.0} {:x 4.600000000000002, :y 96.0} {:x 4.8000000000000025, :y 89.0} {:x 5.000000000000003, :y 91.0} {:x 5.200000000000003, :y 0})}], :width 300, :height 185.4141, :padding {:bottom 40, :top 10, :right 10, :left 55}}}"}],"value":"[#gorilla_repl.vega.VegaView{:content {:axes [{:titleOffset 30, :title \"mean\", :scale \"x\", :type \"x\"} {:titleOffset 45, :title \"sample count\", :scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"linear\", :range \"width\", :zero false, :domain [-2 2]} {:name \"y\", :type \"linear\", :range \"height\", :nice true, :zero false, :domain {:data \"0446bbc0-fb98-4ec7-a481-d1ce0af206fd\", :field \"data.y\"}}], :marks [{:type \"line\", :from {:data \"0446bbc0-fb98-4ec7-a481-d1ce0af206fd\"}, :properties {:enter {:x {:scale \"x\", :field \"data.x\"}, :y {:scale \"y\", :field \"data.y\"}, :interpolate {:value \"step-before\"}, :fill {:value \"steelblue\"}, :fillOpacity {:value 0.4}, :stroke {:value \"steelblue\"}, :strokeWidth {:value 2}, :strokeOpacity {:value 1}}}}], :data [{:name \"0446bbc0-fb98-4ec7-a481-d1ce0af206fd\", :values ({:x -2.0, :y 0} {:x -1.84, :y 12.0} {:x -1.6800000000000002, :y 19.0} {:x -1.5200000000000002, :y 24.0} {:x -1.3600000000000003, :y 37.0} {:x -1.2000000000000004, :y 49.0} {:x -1.0400000000000005, :y 59.0} {:x -0.8800000000000004, :y 117.0} {:x -0.7200000000000004, :y 177.0} {:x -0.5600000000000004, :y 227.0} {:x -0.40000000000000036, :y 354.0} {:x -0.24000000000000035, :y 484.0} {:x -0.08000000000000035, :y 677.0} {:x 0.07999999999999965, :y 917.0} {:x 0.23999999999999966, :y 1439.0} {:x 0.3999999999999997, :y 1937.0} {:x 0.5599999999999997, :y 2491.0} {:x 0.7199999999999998, :y 2986.0} {:x 0.8799999999999998, :y 3149.0} {:x 1.0399999999999998, :y 3043.0} {:x 1.1999999999999997, :y 2549.0} {:x 1.3599999999999997, :y 1756.0} {:x 1.5199999999999996, :y 1101.0} {:x 1.6799999999999995, :y 652.0} {:x 1.8399999999999994, :y 361.0} {:x 1.9999999999999993, :y 179.0} {:x 2.1599999999999993, :y 101.0} {:x 2.3199999999999994, :y 0})}], :width 300, :height 185.4141, :padding {:bottom 40, :top 10, :right 10, :left 55}}} #gorilla_repl.vega.VegaView{:content {:axes [{:titleOffset 30, :title \"sd\", :scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"linear\", :range \"width\", :zero false, :domain [0 5]} {:name \"y\", :type \"linear\", :range \"height\", :nice true, :zero false, :domain {:data \"e53e54dd-6bf5-4e9f-b622-50dd0e5ec19f\", :field \"data.y\"}}], :marks [{:type \"line\", :from {:data \"e53e54dd-6bf5-4e9f-b622-50dd0e5ec19f\"}, :properties {:enter {:x {:scale \"x\", :field \"data.x\"}, :y {:scale \"y\", :field \"data.y\"}, :interpolate {:value \"step-before\"}, :fill {:value \"steelblue\"}, :fillOpacity {:value 0.4}, :stroke {:value \"steelblue\"}, :strokeWidth {:value 2}, :strokeOpacity {:value 1}}}}], :data [{:name \"e53e54dd-6bf5-4e9f-b622-50dd0e5ec19f\", :values ({:x 0.0, :y 0} {:x 0.20000000000000004, :y 0.0} {:x 0.4000000000000001, :y 47.0} {:x 0.6000000000000001, :y 1492.0} {:x 0.8000000000000002, :y 3784.0} {:x 1.0000000000000002, :y 3951.0} {:x 1.2000000000000002, :y 3279.0} {:x 1.4000000000000001, :y 2478.0} {:x 1.6, :y 1883.0} {:x 1.8, :y 1489.0} {:x 2.0, :y 1076.0} {:x 2.2, :y 899.0} {:x 2.4000000000000004, :y 658.0} {:x 2.6000000000000005, :y 579.0} {:x 2.8000000000000007, :y 461.0} {:x 3.000000000000001, :y 342.0} {:x 3.200000000000001, :y 296.0} {:x 3.4000000000000012, :y 255.0} {:x 3.6000000000000014, :y 237.0} {:x 3.8000000000000016, :y 192.0} {:x 4.000000000000002, :y 145.0} {:x 4.200000000000002, :y 125.0} {:x 4.400000000000002, :y 109.0} {:x 4.600000000000002, :y 96.0} {:x 4.8000000000000025, :y 89.0} {:x 5.000000000000003, :y 91.0} {:x 5.200000000000003, :y 0})}], :width 300, :height 185.4141, :padding {:bottom 40, :top 10, :right 10, :left 55}}}]"}
;; <=

;; **
;;; Let's see how the prior on sd looks like:
;; **

;; @@
(plot/histogram (repeatedly N #(/ (sqrt (sample (gamma 1. 1.)))))
                :bins 25 :plot-size 300 :plot-range [[0 5] :all] :x-title "sd")

;; @@
;; =>
;;; {"type":"vega","content":{"axes":[{"titleOffset":30,"title":"sd","scale":"x","type":"x"},{"scale":"y","type":"y"}],"scales":[{"name":"x","type":"linear","range":"width","zero":false,"domain":[0,5]},{"name":"y","type":"linear","range":"height","nice":true,"zero":false,"domain":{"data":"d4cd83fc-7619-4f54-b1b1-ab4b8def5450","field":"data.y"}}],"marks":[{"type":"line","from":{"data":"d4cd83fc-7619-4f54-b1b1-ab4b8def5450"},"properties":{"enter":{"x":{"scale":"x","field":"data.x"},"y":{"scale":"y","field":"data.y"},"interpolate":{"value":"step-before"},"fill":{"value":"steelblue"},"fillOpacity":{"value":0.4},"stroke":{"value":"steelblue"},"strokeWidth":{"value":2},"strokeOpacity":{"value":1}}}}],"data":[{"name":"d4cd83fc-7619-4f54-b1b1-ab4b8def5450","values":[{"x":0.0,"y":0},{"x":0.20000000000000004,"y":0.0},{"x":0.4000000000000001,"y":37.0},{"x":0.6000000000000001,"y":1463.0},{"x":0.8000000000000002,"y":3718.0},{"x":1.0000000000000002,"y":3947.0},{"x":1.2000000000000002,"y":3327.0},{"x":1.4000000000000001,"y":2622.0},{"x":1.6,"y":1933.0},{"x":1.8,"y":1415.0},{"x":2.0,"y":1053.0},{"x":2.2,"y":861.0},{"x":2.4000000000000004,"y":700.0},{"x":2.6000000000000005,"y":559.0},{"x":2.8000000000000007,"y":433.0},{"x":3.000000000000001,"y":332.0},{"x":3.200000000000001,"y":296.0},{"x":3.4000000000000012,"y":274.0},{"x":3.6000000000000014,"y":205.0},{"x":3.8000000000000016,"y":187.0},{"x":4.000000000000002,"y":144.0},{"x":4.200000000000002,"y":152.0},{"x":4.400000000000002,"y":120.0},{"x":4.600000000000002,"y":83.0},{"x":4.8000000000000025,"y":91.0},{"x":5.000000000000003,"y":73.0},{"x":5.200000000000003,"y":0}]}],"width":300,"height":185.41409301757812,"padding":{"bottom":40,"top":10,"right":10,"left":55}},"value":"#gorilla_repl.vega.VegaView{:content {:axes [{:titleOffset 30, :title \"sd\", :scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"linear\", :range \"width\", :zero false, :domain [0 5]} {:name \"y\", :type \"linear\", :range \"height\", :nice true, :zero false, :domain {:data \"d4cd83fc-7619-4f54-b1b1-ab4b8def5450\", :field \"data.y\"}}], :marks [{:type \"line\", :from {:data \"d4cd83fc-7619-4f54-b1b1-ab4b8def5450\"}, :properties {:enter {:x {:scale \"x\", :field \"data.x\"}, :y {:scale \"y\", :field \"data.y\"}, :interpolate {:value \"step-before\"}, :fill {:value \"steelblue\"}, :fillOpacity {:value 0.4}, :stroke {:value \"steelblue\"}, :strokeWidth {:value 2}, :strokeOpacity {:value 1}}}}], :data [{:name \"d4cd83fc-7619-4f54-b1b1-ab4b8def5450\", :values ({:x 0.0, :y 0} {:x 0.20000000000000004, :y 0.0} {:x 0.4000000000000001, :y 37.0} {:x 0.6000000000000001, :y 1463.0} {:x 0.8000000000000002, :y 3718.0} {:x 1.0000000000000002, :y 3947.0} {:x 1.2000000000000002, :y 3327.0} {:x 1.4000000000000001, :y 2622.0} {:x 1.6, :y 1933.0} {:x 1.8, :y 1415.0} {:x 2.0, :y 1053.0} {:x 2.2, :y 861.0} {:x 2.4000000000000004, :y 700.0} {:x 2.6000000000000005, :y 559.0} {:x 2.8000000000000007, :y 433.0} {:x 3.000000000000001, :y 332.0} {:x 3.200000000000001, :y 296.0} {:x 3.4000000000000012, :y 274.0} {:x 3.6000000000000014, :y 205.0} {:x 3.8000000000000016, :y 187.0} {:x 4.000000000000002, :y 144.0} {:x 4.200000000000002, :y 152.0} {:x 4.400000000000002, :y 120.0} {:x 4.600000000000002, :y 83.0} {:x 4.8000000000000025, :y 91.0} {:x 5.000000000000003, :y 73.0} {:x 5.200000000000003, :y 0})}], :width 300, :height 185.4141, :padding {:bottom 40, :top 10, :right 10, :left 55}}}"}
;; <=

;; **
;;; Apparently, in the second model, normal-mean+sd, the standard deviation still follows the prior distribution. In both cases, the mean converged to a posterior, but the posterior in the second case is skewed and has heavier tails. Can someone help me with closed-form analysis?
;; **
