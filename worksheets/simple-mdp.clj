;; gorilla-repl.fileformat = 1

;; **
;;; # Simple MDP
;;; 
;;; We are looking for an example where CtLMH does work.
;; **

;; @@
(ns simple-mdp
  (:require [gorilla-plot.core :as plot])
  (:use [emplan factor])
  (:use [embang runtime emit 
         [core :only [doquery]]
         [state :only [get-predicts]]]))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-nil'>nil</span>","value":"nil"}
;; <=

;; **
;;; Here is a simple MDP:
;;; * two actions;
;;; * four terminal states.
;;; 
;;; We expect the inference to pick action B most of the time.
;; **

;; @@
(defquery simple-mdp temperature
  (let [a (sample :action (categorical [[:a 1] [:b 1]]))
        s (sample (with-meta [a] {:noise true}) 
                  (discrete (get {:a [0.2 0.8 0.0 0.0]
                                  :b [0.0 0.0 0.7 0.6]}
                                 a)))]
    (observe +factor+ (/ (get [1.0 0.0 0.1 0.3] s) temperature))
    (predict :a a)
    (predict :s s)))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;simple-mdp/simple-mdp</span>","value":"#'simple-mdp/simple-mdp"}
;; <=

;; @@
(def N 1000)
(def T 10)
(def predicts (map get-predicts (take N (drop N (take-nth T (doquery :ctlmh simple-mdp 0.1))))))
;; @@
;; =>
;;; {"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"html","content":"<span class='clj-var'>#&#x27;simple-mdp/N</span>","value":"#'simple-mdp/N"},{"type":"html","content":"<span class='clj-var'>#&#x27;simple-mdp/T</span>","value":"#'simple-mdp/T"}],"value":"[#'simple-mdp/N,#'simple-mdp/T]"},{"type":"html","content":"<span class='clj-var'>#&#x27;simple-mdp/predicts</span>","value":"#'simple-mdp/predicts"}],"value":"[[#'simple-mdp/N,#'simple-mdp/T],#'simple-mdp/predicts]"}
;; <=

;; @@
(def afreqs (sort-by first (frequencies (map (juxt :a) predicts))))
(plot/bar-chart (map first afreqs) (map second afreqs) )
;; @@
;; =>
;;; {"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"html","content":"<span class='clj-var'>#&#x27;simple-mdp/afreqs</span>","value":"#'simple-mdp/afreqs"},{"type":"vega","content":{"axes":[{"scale":"x","type":"x"},{"scale":"y","type":"y"}],"scales":[{"name":"x","type":"ordinal","range":"width","domain":{"data":"b8775ce5-883a-4200-866d-c7c920189450","field":"data.x"}},{"name":"y","range":"height","nice":true,"domain":{"data":"b8775ce5-883a-4200-866d-c7c920189450","field":"data.y"}}],"marks":[{"type":"rect","from":{"data":"b8775ce5-883a-4200-866d-c7c920189450"},"properties":{"enter":{"y":{"scale":"y","field":"data.y"},"width":{"offset":-1,"scale":"x","band":true},"x":{"scale":"x","field":"data.x"},"y2":{"scale":"y","value":0}},"update":{"fill":{"value":"steelblue"},"opacity":{"value":1}},"hover":{"fill":{"value":"#FF29D2"}}}}],"data":[{"name":"b8775ce5-883a-4200-866d-c7c920189450","values":[{"x":["a"],"y":366},{"x":["b"],"y":634}]}],"width":400,"height":247.2187957763672,"padding":{"bottom":40,"top":10,"right":10,"left":55}},"value":"#gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"b8775ce5-883a-4200-866d-c7c920189450\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"b8775ce5-883a-4200-866d-c7c920189450\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"b8775ce5-883a-4200-866d-c7c920189450\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"b8775ce5-883a-4200-866d-c7c920189450\", :values ({:x [:a], :y 366} {:x [:b], :y 634})}], :width 400, :height 247.2188, :padding {:bottom 40, :top 10, :right 10, :left 55}}}"}],"value":"[#'simple-mdp/afreqs,#gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"b8775ce5-883a-4200-866d-c7c920189450\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"b8775ce5-883a-4200-866d-c7c920189450\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"b8775ce5-883a-4200-866d-c7c920189450\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"b8775ce5-883a-4200-866d-c7c920189450\", :values ({:x [:a], :y 366} {:x [:b], :y 634})}], :width 400, :height 247.2188, :padding {:bottom 40, :top 10, :right 10, :left 55}}}]"}
;; <=
