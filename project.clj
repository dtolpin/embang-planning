(defproject embang/planning "0.1.0-SNAPSHOT"
  :description "m! modules for planning and policy learning"
  :url "https://bitbucket.org/dtolpin/emplan"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [embang "1.0.0-SNAPSHOT"]
                 [net.mikera/core.matrix.stats "0.5.0"]]
  :plugins [[dtolpin/lein-gorilla "0.3.5-SNAPSHOT"]])
