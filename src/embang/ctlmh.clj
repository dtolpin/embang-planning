(ns embang.ctlmh
  "Control LMH"
  (:refer-clojure :exclude [rand rand-int rand-nth])
  (:require [embang lmh])
  (:use [embang 
         [state :only [set-log-weight]]
         inference
         lmh]))

;;; Lightweight Metropolis-Hastings for Optimal Control
;;
;; Just like regular LMH, but always accept if a change
;; is proposed to a noise random choice. Used for optimal
;; control in POMDPs.
;;
;; To mark a choice as noise, attach meta-data {:noise true}
;; to the choice's identifier (first parameter of sample).

(derive ::algorithm ::embang.lmh/algorithm)

(defn noise? 
  "true when the choice is noise"
  [choice-id]
  (:noise (meta (first choice-id))))

(defmethod infer :ctlmh [_ prog value & {}]
  (letfn
    [(sample-seq [state]
       (lazy-seq
         (let [;; Choose uniformly a random choice to resample.
               entry (rand-nth (get-trace state))
               ;; Compute next state from the resampled choice.
               next-state (next-state state entry)
               ;; Reconstruct the current state through transition back
               ;; from the next state; the rdb will be different.
               prev-state (prev-state state next-state entry)
               ;; Apply Metropolis-Hastings acceptance rule to select
               ;; either the new or the current state.
               state (cond
                       ;; Always accept proposed noise.
                       (noise? (:choice-id entry))
                       next-state
                       ;; Otherwise accept with MH propability.
                       (> (- (utility next-state)
                             (utility prev-state))
                          (Math/log (rand)))
                       next-state
                       :else state)]
           ;; Include the selected state into the sequence of samples,
           ;; setting the weight to the unit weight.
           (cons (set-log-weight state 0.) (sample-seq state)))))]

    (let [state (:state (exec ::algorithm prog value initial-state))]
      (if (seq (get-trace state))
        (sample-seq state)
        ;; No randomness in the program.
        (repeat (set-log-weight state 0.))))))
