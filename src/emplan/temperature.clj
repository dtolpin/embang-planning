(ns emplan.temperature
  "Temperature for mode inference")

(defprotocol temperature
  (current [this] "reference to the current temperature")
  (decrease [this] "decrease temperature"))

(defn make-temperature
  "creates temperature source"
  ([initial-temperature]
   (make-temperature initial-temperature identity))
  ([initial-temperature decrease*]
    (let [tref (atom initial-temperature)]
      (reify temperature
        (current [this] (deref tref))
        (decrease [this] (swap! tref decrease*))))))
