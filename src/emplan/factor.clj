(ns emplan.factor
  "Maintaining probabilities for planning and policy learning"
  (:use [embang runtime emit]))

;; Policy learning relies on the `factor' pseudo-distribution
;; to manipulate log weights.

(defdist factor
  "factor pseudo-distribution;
  `sample' returns zero,
  `observe' returns the value as the log-probability."
  []
  (sample [this] 0.0)
  (observe [this value] value))

;; Since `factor' takes no parameters, we define singleton
;; +factor+ to save on re-creating the distribution object.

(def +factor+ "factor singleton" (factor))
